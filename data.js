const NUM_OF_STUDENTS = 100;

const courses = [
  "Programming Principles I",
  "Programming Principles II",
  "Data Structures & Algorithms",
  "Introduction to Computer Networks",
  "Computer Organization & Architecture",
  "Theory of Computation",
  "Principles of Operating Systems",
  "Database Systems",
  "Computer Graphics",
  "Object Oriented Analysis & Design",
];

const teachers = [
  "Farid Ahmadov",
  "Emin Alasgarov",
  "Emil Abbasov",
  "Mykhailo Medvediev",
  "Fadai Ganjaliyev",
];

function generateRandomStudentsData(len) {
  return new Array(len).fill(0).map(() => {
    return Math.floor(Math.random() * (len + 1)) + 1;
  });
}

function generateRandomColorArray(len) {
  return new Array(len).fill(0).map(() => {
    const r = Math.floor(Math.random() * 256);
    const g = Math.floor(Math.random() * 256);
    const b = Math.floor(Math.random() * 256);

    return `rgb(${r}, ${g}, ${b})`;
  });
}
