const studentsPerCourseCtx = document
  .getElementById("students-per-course")
  .getContext("2d");

const studentsPerTeacherCtx = document
  .getElementById("students-per-teacher")
  .getContext("2d");

const teachingHoursPerCourseCtx = document
  .getElementById("teaching-hours-per-course")
  .getContext("2d");

const teachingHoursPerTeacherCtx = document
  .getElementById("teaching-hours-per-teacher")
  .getContext("2d");

new Chart(studentsPerCourseCtx, {
  type: "bar",
  data: {
    labels: courses,
    datasets: [
      {
        label: "Courses",
        backgroundColor: generateRandomColorArray(NUM_OF_STUDENTS),
        data: generateRandomStudentsData(NUM_OF_STUDENTS),
      },
    ],
  },
});

new Chart(studentsPerTeacherCtx, {
  type: "pie",
  data: {
    labels: teachers,
    datasets: [
      {
        backgroundColor: generateRandomColorArray(teachers.length),
        data: generateRandomStudentsData(teachers.length),
      },
    ],
  },
});

new Chart(teachingHoursPerCourseCtx, {
  type: "radar",
  data: {
    labels: courses,
    datasets: [
      {
        label: "Teaching hours",
        backgroundColor: "rgba(255, 84, 84, 0.2)",
        borderColor: "rgb(255, 84, 84)",
        data: generateRandomStudentsData(courses.length),
      },
    ],
  },
});

new Chart(teachingHoursPerTeacherCtx, {
  type: "doughnut",
  data: {
    labels: teachers,
    datasets: [
      {
        backgroundColor: generateRandomColorArray(teachers.length),
        data: generateRandomStudentsData(teachers.length),
      },
    ],
  },
});
